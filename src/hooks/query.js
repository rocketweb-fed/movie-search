import { useState } from 'react'
import { useHistory } from 'react-router-dom'

export const useQuery = () => {
    const [searchQuery, updateSearchQuery] = useState({ term: '', page: 1 })
    const history = useHistory()
    
    if (history.location.state) {
        if (history.location.state.query.term !== searchQuery.term || history.location.state.query.page !== searchQuery.page) {
            updateSearchQuery({
                term: history.location.state.query.term,
                page: history.location.state.query.page
            })
        }
    }

    return searchQuery
}