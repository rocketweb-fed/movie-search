const createMovieDbUrl = (relativeUrl, queryParams) => {
    let baseUrl = `${process.env.REACT_APP_MOVIE_DB_BASE_URL}${relativeUrl}?api_key=${process.env.REACT_APP_MOVIE_DB_API_KEY}&language=en-US`;
    if (queryParams) {
        Object.keys(queryParams)
            .forEach(paramName => baseUrl += `&${paramName}=${queryParams[paramName]}`);
    }
    return baseUrl;
}

export const getTopMoviesUrl = ({page}) => {
    const fullUrl = createMovieDbUrl('/movie/top_rated', {
        page
    })
    return fullUrl
}

export const searchMoviesUrl = ({ page, query }) => {
    return createMovieDbUrl('/search/movie', {
        page,
        query
    })
}

export const getMovieDetailsUrl = ({ movieId }) => {
    return createMovieDbUrl(`/movie/${movieId}`)
}