const TMDB_IMAGE_BASE_URL = (width = 300) => `https://image.tmdb.org/t/p/w${width}`

export const updateMoviePictureUrls = (movie, width = 300) => (
    `${TMDB_IMAGE_BASE_URL(width)}${movie.poster_path}`
)