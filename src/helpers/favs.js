const checkFav = (favs, movie) => {
    return favs.some(fav => fav.id === movie.id)
}

export const getIsFav = (favs, movie) => {
    return checkFav(favs, movie)
}

export const updateFavs = (favs, dispatch, movie) => {
    const isFav = checkFav(favs, movie)

    if (isFav) {
        dispatch({ type: 'removeFromFavs', remove: movie.id })
    } else {
        dispatch({ type: 'addToFavs', add: movie })
    }
}

export const clearFavs = (dispatch) => {
    dispatch({ type: 'clearFavs' })
}