import { getTopMoviesUrl, searchMoviesUrl, getMovieDetailsUrl } from '../services/tmdb'

export const getTopMovies = (page) => {
    return getTopMoviesUrl({ page: page })
}

export const searchMovies = (page, query) => {
    return query ? searchMoviesUrl({ page: page, query: query }) : null
}

export const getMovieDetails = (movieId) => {
    return getMovieDetailsUrl({movieId: movieId})
}

export const getMovieReleaseYear = (releaseDate) => {
    return releaseDate.substr(0, 4)
}