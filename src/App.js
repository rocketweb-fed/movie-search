import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import './App.css'

import Header from  './components/header'
import MovieDetails from './components/movie/details'
import MovieSearch from './components/movie/search'
import MovieList from './components/movie/list'
import FavsList from './components/fav/list'
import { FavsProvider } from './components/fav/context'

const App = () => {
  const initialState = [];

  const reducer = (state, action) => {
    switch (action.type) {
      case 'addToFavs':
      return [
        ...state,
        action.add
      ]
      case 'removeFromFavs':
        return state.filter(fav =>  { 
          return fav.id !== action.remove 
        })
        case 'clearFavs': 
          return []
      default:
        return state
    }
  };

  return (
    <Router>
      <div className="App">
        <FavsProvider initialState={initialState} reducer={reducer}>
          <Header />
          <Switch>
              <Route path="/movie/:id" exact>
                <MovieDetails />
              </Route>
              <Route path="/favs">
                <FavsList />
              </Route>
              <Route path="/">
                <MovieSearch />
                <MovieList />
              </Route>
          </Switch>
        </FavsProvider>
      </div>
    </Router>
  );
}

export default App
