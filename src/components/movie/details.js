import React from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { updateMoviePictureUrls } from '../../helpers/image'
import { useHttp } from '../../hooks/http'
import { getMovieDetails, getMovieReleaseYear } from '../../helpers/movie'
import Loader from '../loader'
import Button from '../button'
import MoviePlaceholder from '../placeholders/moviePlaceholder'
import { useFavs } from '../fav/context'
import { getIsFav, updateFavs } from '../../helpers/favs'

const styles = {
    bgImage: {
        width: '100%',
        height: 'auto'
    },
    description: {
        padding: '0 15px 20px'
    }
};

const MovieDetails = () => {
    const { id } = useParams()
    const history = useHistory()
    const [favs, dispatch] = useFavs()
    const [loading, movie] = useHttp(getMovieDetails(id), [])
    
    if (loading) { return <Loader /> }
    if (!movie) { return <MoviePlaceholder /> }

    let isFav = getIsFav(favs, movie)

    return (
        <article>
            <img style={styles.bgImage} alt={movie ? movie.title : ''} src={movie ? updateMoviePictureUrls(movie) : ''} />
            <div style={styles.description}>
                <h2>{movie ? movie.title : ''} <em>({movie ? getMovieReleaseYear(movie.release_date) : ''})</em></h2>
                <p>{movie ? movie.overview : ''}</p>
                <Button onClick={() => updateFavs(favs, dispatch, movie)} type={isFav ? 'button' : 'fav' }>{isFav ? 'Remove from ' : 'Add to ' }Watch List</Button>
                <Button onClick={() => history.goBack()}>Back to listing</Button>
            </div> 
        </article>
    )
}

export default MovieDetails