import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import Button from '../button'

const styles = {
    input: {
        borderWidth: '0 0 1px',
        borderStyle: 'solid',
        borderColor: '#a3a3a3',
        padding: '.7em 1em',
        marginRight: '1em'
    },
    searchbox: {
        marginBottom: '2em'
    }
}

const MovieSearch = () => {
    const [query, updateQuery] = useState({term: '', page: 1})
    const history = useHistory()
    
    const handleChange = (ev) => {
        updateQuery({
            term: ev.target.value,
            page: query.page
        })
    }

    const handleSubmit = (ev) => { 
        ev.preventDefault()
        history.push(`?search=${query.term}&page=${query.page}`, { query })
    }

    return (
        <div>
            <h2>Movie Search</h2>
            <div>
                <form style={styles.searchbox} onSubmit={ handleSubmit }>
                    <input style={styles.input} type="text" placeholder="Search for a movie" onChange={ handleChange } />
                    <Button type="submit">Search</Button>
                </form>
            </div>
        </div>
    )
}

export default MovieSearch