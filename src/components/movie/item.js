import React from 'react'
import { Link } from 'react-router-dom'
import { updateMoviePictureUrls } from '../../helpers/image'
import FavButton from '../fav/star'


const styles = {
    movieItem: {
        flex: '1 1 50%',
        position: 'relative'
    },
    bgImage: {
        width: '100%'
    }
};

const MovieItem = props => {
    const { movie } = props

    const moviePoster = updateMoviePictureUrls(movie)

    return (
        <article style={styles.movieItem}>
            <FavButton movie={movie} />
            <Link to={`movie/${movie.id}`}>
                <img style={styles.bgImage} alt={movie.title} src={moviePoster} />
                <h3>{movie.title}</h3>
            </Link>
        </article>
    )
}

export default MovieItem