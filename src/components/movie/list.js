import React from 'react'
import MovieItem from './item'
import Pagination from '../pagination'
import Loader from '../loader'
import { searchMovies } from '../../helpers/movie'
import { useHttp } from '../../hooks/http'
import { useQuery } from '../../hooks/query'

const styles = {
    movieList: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    text: {
        width: '100%',
        textAlign: 'center'
    }
};

const MovieList = () => {
    const query = useQuery()
    const [ isLoading, movies ] = useHttp(searchMovies(query.page, query.term), [query])
    
    const movieList = isLoading ? <Loader /> : movies && movies.results.length > 0 ? movies.results.map(movie => (
        <MovieItem movie={movie} key={movie.id} />
    )) : <p style={styles.text}>No movies found</p>

    return (
        <div>
            <Pagination total={ movies ? movies.total_pages : 0 } current={ movies ? movies.page : 0 } />
            <div style={styles.movieList}>
                { movieList }
            </div>
            <Pagination total={movies ? movies.total_pages : 0} current={movies ? movies.page : 0} />
        </div>
    )
}

export default MovieList