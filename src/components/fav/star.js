import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { useFavs } from './context'
import { getIsFav, updateFavs } from '../../helpers/favs'

const FavButton = ({movie}) => {
    const [favs, dispatch] = useFavs()
    let isFav = getIsFav(favs, movie)

    return (
        <div onClick={() => updateFavs(favs, dispatch, movie)} style={{position: 'absolute', top: '1em', right: '1em'}}>
            <FontAwesomeIcon icon={faStar} style={{fontSize: '2em', color: isFav ? 'yellow' : 'gray'}} />
        </div>
    )
}

export default FavButton
