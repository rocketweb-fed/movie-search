import React, { createContext, useReducer, useContext } from 'react'

export const FavsContext = createContext()

export const FavsProvider = ({ reducer, initialState, children }) => {
    return (
        <FavsContext.Provider value={useReducer(reducer, initialState)}>
            {children}
        </FavsContext.Provider>
    );
}

export const useFavs = () => useContext(FavsContext);