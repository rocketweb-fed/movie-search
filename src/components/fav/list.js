import React from 'react'
import { useHistory } from 'react-router-dom'
import MovieItem from '../movie/item'
import Button from '../button'
import { useFavs } from './context'
import { clearFavs } from '../../helpers/favs'

const styles = {
    movieList: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    text: {
        width: '100%',
        textAlign: 'center'
    }
};

const FavsList = () => {
    const history = useHistory()
    const [favs, dispatch] = useFavs()
    const favsList = favs && favs.length > 0 ? favs.map(movie => (
        <MovieItem movie={movie} key={movie.id} />
    )) : <p style={styles.text}>No movies in Watch List</p>

    return (
        <div>
            <div style={styles.movieList}>
                { favsList }
            </div>
            <Button onClick={() => clearFavs(dispatch)} type="button">Clear Watch List</Button>
            <Button onClick={() => history.goBack()}>Go back</Button>
        </div>
    )
}

export default FavsList