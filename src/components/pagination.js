import React from 'react'
import { Link } from 'react-router-dom'
import { useQuery } from '../hooks/query'

const styles = {
    active: {
        color: 'limegreen'
    },

    pagination: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        marginBottom: '1rem'
    },

    pageLink: {
        display: 'block',
        padding: '.3rem .5rem',
        border: 'solid 1px gray',
        borderRadius: '.2rem',
        margin: '.3rem .5rem'
    }
}

const Page = (props) => {
    return (
        <Link to={{
            search: `?search=${props.query.term}&page=${props.query.page}`,
            state: { query: props.query }
        }} style={styles.pageLink}>
            <span style={props.active ? styles.active : null}>{ props.page }</span>
        </Link>
    )
}

const Pagination = (props) => {
    const {total, current} = props
    const query = useQuery()

    let pages = []

    for (let p = 0; p < total; p++) {
        pages[p] = {
            index: p + 1,
            active: (p + 1 === current) ? true : false
        }    
    }

    const pagination = pages ? pages.filter(page => page.index < 9).map((page) => <Page page={ page.index } active={ page.active } query={{term: query.term, page: page.index}} />) : '' 

    return (
        <div style={styles.pagination}>
            { pagination }
        </div>
    )
}

export default Pagination