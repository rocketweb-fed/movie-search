import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

const styles = {
    container: { 
        display: 'flex', 
        width: '100%',
        justifyContent: 'center' 
    }
}

const Loader = () => {
    return (
        <div style={styles.container}>
            <FontAwesomeIcon icon={faSpinner} size="5x" spin style={{color: '#a4a2a5'}} />
        </div>
    )
}

export default Loader