import React from 'react'

const MoviePlaceholder = () => {
    const styles={
        imagePlaceholder: {
            display: 'block',
            width: '100%',
            height: '35em',
            background: '#ccc'
        },

        titlePlaceholder: {
            display: 'inline-block',
            width: '60%',
            height: '2em',
            margin: '1rem',
            background: '#ccc'
        },

        overviewPlaceholder: {
            display: 'inline-block',
            width: '90%',
            height: '10em',
            background: '#ccc'
        },
    }

    return (
        <div>
            <span style={styles.imagePlaceholder}></span>
            <span style={styles.titlePlaceholder}></span>
            <span style={styles.overviewPlaceholder}></span>
        </div>
    )
}

export default MoviePlaceholder
