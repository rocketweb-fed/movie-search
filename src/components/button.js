import React from 'react'
import { useHistory } from "react-router-dom";

const Button = (props) => {
    const {
        type,
        ...rest
    } = props

    const styles = {
        button: {
            borderRadius: '5px',
            background: type && type === 'fav' ? 'limegreen' : '#d3d4d3', 
            padding: '.5rem 2rem',
            textTransform: 'uppercase',
            fontWeight: 'bold'
        }
    }

    return (
        <button {...rest} type={type || "button"} style={styles.button} />
    )
}

export const LinkButton = (props) => {
    let history = useHistory()

    const {to, ...rest} = props

    const styles = {
        button: {
            borderRadius: '5px',
            background: 'limegreen',
            padding: '.5rem 2rem',
            textTransform: 'uppercase',
            fontWeight: 'bold'
        }
    }

    return (
        <button
            {...rest}
            onClick={() => { history.replace(to) }}
            style={styles.button}
        />
    )
}

export default Button