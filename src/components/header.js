import React from 'react'
import { Link } from 'react-router-dom'

const styles = {
    headerLinks: {
        display: 'flex',
        justifyContent: 'flex-end',
        padding: '.5rem 1rem'
    }
};

const Header = () => {
    return (
        <div style={styles.headerLinks}>
            <Link to="/favs">Watch List</Link>
        </div>
    )
}

export default Header